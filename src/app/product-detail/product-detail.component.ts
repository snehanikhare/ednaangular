import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../model/product';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Order } from '../model/Order';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  @Input() selectedProduct: Product;
  msgString: any;
  authToken: any;
  orderMongo : Order = new Order();
  userId: any;
  errString: any;
  title = "Product Details";
  myForm: FormGroup;
  isQtyValid: boolean;
  qtyRangeErr: boolean;

  constructor(private productService: ProductService, 
    private route: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.isQtyValid = true;
    // console.log('selectedProduct', this.selectedProduct)
    this.msgString = "";
    this.userId = localStorage.getItem('userId');
    this.authToken  = localStorage.getItem('authToken'); 
    this.myForm = this.formBuilder.group({
      orderQuantity: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.maxLength(2), Validators.max(99), Validators.min(1)]]
    });
  }

  placeOrder(productId:string,productTitle:string,orderQuantity:number) {
    //console.info(this.chkPlaceOrder);
    this.isQtyValid = true;
    this.qtyRangeErr = false;
    if(orderQuantity && orderQuantity <= this.selectedProduct.productQuantity && orderQuantity > 0) {     
        
        const authToken = localStorage.getItem('authToken');
        if (authToken) {
          this.orderMongo.userId=this.userId;  
          this.orderMongo.productId=productId;  
          this.orderMongo.quantity=orderQuantity;   
          this.productService.placeOrder(this.orderMongo, authToken).subscribe(data =>{
          this.msgString = data['message'];
          this.selectedProduct = null;
          this.route.navigate(['/'], { queryParams: { msgString: this.msgString } });
        },
        (error) => {                              //Error callback
          console.error('error caught in component', error)
          this.errString = error.message;
        });
      }else if(authToken) {
        this.selectedProduct = null;
        this.route.navigate(['/']);
      } else if(!authToken) {
        this.route.navigate(['/login-user']);
      }
    } else {
      this.showQuantityErr(orderQuantity);
    }
  }


  showQuantityErr(qty){
    if(!qty || qty == '') {
      this.isQtyValid = false;
    } else if(qty > this.selectedProduct.productQuantity || qty <= 0) {
      this.qtyRangeErr = true;
    } 
  }

}
