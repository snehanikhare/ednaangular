export interface Product {
  productId: string,		
  productTitle: string,
  productQuantity: number,
	productDesc: string,
	productPrice: string,
	productImg: string
}