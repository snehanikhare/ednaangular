export class OrderResponse {
    
	status:string;
	productId: string;
    orderId: string;
    message: string;
    quantity: number;  

}