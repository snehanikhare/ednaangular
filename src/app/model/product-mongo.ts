export interface ProductMongo {
    productId:string,
    productTitle:string,
    productQuantity:number,
    productDesc:string,
    productPrice:string,
    productImg:string
}
