export class Order {
    orderId?: string;
    userId: string;
    productId: string;
    quantity: number;
}