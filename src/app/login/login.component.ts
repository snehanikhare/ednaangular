import { LoginService } from './../services/LoginService';

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  

  loginForm:FormGroup;
  submitted: boolean=false;
  invalidLogin: boolean=false;
  errString: string;
  constructor(private formBuilder: FormBuilder, private loginService: LoginService,private router: Router) { }
  ngOnInit() {   
    localStorage.getItem('userId') ? this.router.navigateByUrl('') : this.router.navigateByUrl('login-user');
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(){
    this.submitted = true;
    if(this.loginForm.invalid){
      return;
    }
    console.log("Hello world!"+this.loginForm.value);
    this.loginService.authenticate(this.loginForm.value, () => {
      let returnUrl = localStorage.getItem('stateUrl');
      if(returnUrl) {
        returnUrl = decodeURIComponent(returnUrl);
        this.router.navigateByUrl(returnUrl);
        localStorage.removeItem('stateUrl');
      } else {
        this.router.navigate(['/']);
      }
  }, 
  () => {
    console.log('unauthorised....');
    this.errString = "Invalid credentials entered";
  });
  return false;
}

  
}
