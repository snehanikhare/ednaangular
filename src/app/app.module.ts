import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchProductComponent } from './search-product/search-product.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { LoginComponent } from './login/login.component';
import { DataService } from './services/data.service';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { OrderComponent} from './order/order.component';
import { OrderService} from './services/order.service';
import { AddProductComponent } from './add-product/add-product.component';
import { AuthGuardService } from './services/auth-guard.service';
import { LoginService } from './services/LoginService';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { NgxSpinnerModule } from "ngx-spinner";  
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    SearchProductComponent,
    HeaderNavComponent,
    RegisterUserComponent,
    LoginComponent,
    HomeComponent,
    ProductComponent,
    OrderComponent,
    AddProductComponent,
    ProductDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
  ],
  providers: [
    DataService, 
    OrderService, 
    AuthGuardService,
    LoginService
  ],
    
  bootstrap: [AppComponent]
})
export class AppModule { }
