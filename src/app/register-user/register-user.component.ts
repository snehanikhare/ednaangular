import { Component, OnInit } from '@angular/core';
import { User } from './model/User';
import { RegisterService } from './service/register-service.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  userFormObj:User= new User();

  userList: any;
  userForm: FormGroup;
  constructor(private registerService: RegisterService,
    private router: Router,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    localStorage.getItem('userId') ? this.router.navigateByUrl('') : this.router.navigateByUrl('register-user');
    this.userForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[a-zA-Z0-9 ]*') ]],
      password: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9 ]*'), Validators.minLength(6), Validators.max(10)]],
      address: ['', [Validators.required]],
      city: ['', [Validators.required]],
      category: ['', [Validators.required]]
    });
    this.getUsers();
  }
  getUsers() {
    this.registerService.getUsers()
    .then((response: any) => {
      console.log(response);
      this.userList = response;
    });
  }
  
  
  saveUser(){
    if (!this.userForm.invalid){
      console.log(this.userFormObj);
      this.registerService.createUser(this.userFormObj)
        .then((response: any) => {
            alert("User Created Successfully!");
            this.router.navigateByUrl('/login-user');
        });
    }
    

      
  }
}
