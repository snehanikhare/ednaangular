import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
 

  url = environment.baseUrl + environment.webUrl.users;
  constructor(private http: HttpClient) { }

  createUser(userFormObj): Promise<Object> {
    return this.http.post(this.url +"/createUser", userFormObj).toPromise();
  }

  getUsers() {
    const url = environment.baseUrl + environment.webUrl.users + "/users";
    return this.http.get(url).toPromise();
  }
}
