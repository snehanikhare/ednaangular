import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  title = "My Orders"
  orderMongolist: any[] = [];
  userId: string;
  totCount: number;
  searchText: string;
  noRec: boolean = false;
  productDetail: any;
  authToken: any;
  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.userId = localStorage.getItem('userId') ? localStorage.getItem('userId') : '1';
    this.authToken = localStorage.getItem('authToken');
    this.getOrdersList();
  }

  //Fetches all Orders placed
  getOrdersList() {
    this.searchText = '';
    this.totCount = 0;
    this.noRec = false;
    this.orderService.getOrdersList(this.userId, this.authToken).subscribe(data => {
      this.orderMongolist = data;
      this.totCount = this.orderMongolist ? this.orderMongolist.length : 0;
    });
  }


  searchOrder(searchText: string) {
    if (searchText == '' || searchText.trim() == '') {
      alert("Please enter search text");
    } else {
      this.orderMongolist = this.orderMongolist.filter(ord => searchText == ord.orderId);
      if (this.orderMongolist.length != 0) {
        this.totCount = this.orderMongolist ? this.orderMongolist.length : 0;
      } else {
        this.totCount = 0;
        this.noRec = true;
      }
    }
  }


}