import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';
import { LoginService } from '../services/LoginService';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent implements OnInit {

  isAuthenticated$: Observable<boolean>;
  isLoggedin$: Observable<boolean>;
  constructor(private loginService: LoginService) { 
    this.isAuthenticated$ = this.loginService.loggedIn$;
  }

  ngOnInit() { 
    if(localStorage.getItem('userId')) {      
      this.loginService.loggedIn$.next(true);
    }
  }
  
  logout() { 
    this.loginService.logout(); 
  }
}
