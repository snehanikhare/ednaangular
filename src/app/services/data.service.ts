import { Injectable } from '@angular/core';
import { Product } from '../model/product';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  searchOption=[];
  public productsData: Product[];
  user: Observable<any>;
  // productUrl : string = "http://52.14.121.108:8084/eDNA/product/getProducts";
  // addProductUrl : string = "http://52.14.121.108:8084/eDNA/product/addProductMongo";
  constructor(private http: HttpClient) { 
    this.user = Observable.bind({user:"snikhare"})
  }
  
  getProducts(): Observable<Product[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        // 'authorization':authToken,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,PUT',
        'Access-Control-Allow-Headers': 'content-type'
      })
    };
    return this.http.get<Product[]>(environment.baseUrl + environment.webUrl.getProducts, httpOptions);
  }

  addProduct(product: Product, authToken:string): Observable<string> {
    const httpOptions = {
      headers: new HttpHeaders({
        'authorization':authToken,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,PUT',
        'Access-Control-Allow-Headers': 'content-type'
      })
    };
    return this.http.post<string>(environment.baseUrl + environment.webUrl.addProduct, product, httpOptions);
  }

  searchProductByTitle(productTitle: string, authToken:string): Observable<Product[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'authorization':authToken,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,PUT',
        'Access-Control-Allow-Headers': 'content-type'
      })
    };
    return this.http.get<Product[]>(environment.baseUrl + environment.webUrl.getProductsByTitle + '/' + productTitle, httpOptions);
  }
}
