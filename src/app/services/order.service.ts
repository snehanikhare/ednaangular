import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class OrderService {

  constructor(private http: HttpClient) { }

  //Get Orders List
  getOrdersList(userId: string, authToken: string) {
    console.log(authToken);
    const httpOptions = {
      headers: new HttpHeaders({
        'authorization': authToken,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,PUT',
        'Access-Control-Allow-Headers': 'content-type'
      })
    };
    console.log("resource is  :: " + environment.baseUrl + environment.webUrl.listOrdersbyUserId + userId + "HttpOptions:: " + httpOptions);
    return this.http.get<any[]>(environment.baseUrl + environment.webUrl.listOrdersbyUserId + userId, httpOptions);

  }
}
