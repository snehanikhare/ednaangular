import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'GET,POST',
  'Access-Control-Allow-Headers':'content-type'
  })
 };
@Injectable({
  providedIn: 'root'
})
export class LoginService {


  authenticated = false;
  loggedIn$ = new BehaviorSubject<boolean>(false); 
  loginURL: string = environment.baseUrl + environment.webUrl.login;

  constructor(private http: HttpClient, private route: Router) { } 
  
  authenticate(credentials, callback, errcallback) {
    const headers = new HttpHeaders(credentials ? {
      'authorization': 'Basic ' + btoa(credentials.email + ':' + credentials.password),
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'authorization, content-type, xsrf-token, Cache-Control'      
    } : {});
    
    console.log("Credentials :: " + credentials.email + 'Password: '+credentials.password);    
    this.http.get(this.loginURL,{ headers: headers, responseType: 'text' }).subscribe(response => {
      console.log("after getting response from backend :: " + response);
      if (response) {
        this.authenticated = true;
        localStorage.setItem('userId', response);
        localStorage.setItem('userName', response);
        localStorage.setItem('authToken', 'Basic ' + btoa(credentials.email + ':' + credentials.password));
        this.loggedIn$.next(true);
      } else {
        this.authenticated = false;
        this.loggedIn$.next(false);
      }
      console.log("AUTH ::::: "+this.authenticated);
      return callback && callback();
      // return response;
    },
    (error) => {                              //Error callback
      console.error('error caught in component', error)
      if(error.status === 401) {
        return errcallback && errcallback();
      }
    });

}

  public isAuthenticated(): boolean {
    const userId = localStorage.getItem('userId');
    const authToken = localStorage.getItem('authToken');
    if (userId && this.authenticated && authToken)
      return true;

    return false;
  }

  
  public logout() {
    localStorage.removeItem('userId'); 
    localStorage.removeItem('authToken');     
    localStorage.removeItem('userName'); 
    this.authenticated=false;
    this.loggedIn$.next(false);  
    this.route.navigateByUrl('');
  }
 
}



