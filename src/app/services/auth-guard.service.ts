import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './LoginService';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private userLogin: LoginService,
    private router: Router) { }

  canActivate(route, state: RouterStateSnapshot): boolean {
    if (!localStorage.getItem('userId')) { // && user.userId
      localStorage.setItem('stateUrl', state.url);
      this.router.navigate(['/login-user']);
        return false;
      } else {
        return true;
      }
    }
    
  }

  

