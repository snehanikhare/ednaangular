import { Injectable } from '@angular/core';
import { Order } from '../model/Order';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductMongo } from '../model/product-mongo';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,POST,PUT',
    'Access-Control-Allow-Headers': 'content-type'
  })
};
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private httpClient: HttpClient) { }
 
  getAllProducts(authToken:string): Observable<ProductMongo[]> {
    console.log(authToken);
    const httpOptions = {
      headers: new HttpHeaders({
        'authorization':authToken,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,PUT',
        'Access-Control-Allow-Headers': 'content-type'
      })
    };
    console.log("resource is  :: " + environment.baseUrl + environment.webUrl.getProducts +"HttpOptions:: "+httpOptions); 
    return this.httpClient.get<any[]>(environment.baseUrl + environment.webUrl.getProducts, httpOptions);
  }
  placeOrder(placOrder:Order,authToken:string)
  {
    console.log("authToken :: "+authToken);
    const httpOptions = {
      headers: new HttpHeaders({
        'authorization':authToken,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,PUT',
        'Access-Control-Allow-Headers': 'content-type'
      })
    };
    console.log("resource is  :: " + environment.baseUrl + environment.webUrl.placeOrder +"HttpOptions:: "+httpOptions); 
    return this.httpClient.post<any[]>(environment.baseUrl + environment.webUrl.placeOrder,placOrder, httpOptions);
  }
}
