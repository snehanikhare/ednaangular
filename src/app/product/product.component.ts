import { Component, OnInit } from '@angular/core';
import { Order } from '../model/Order';
import { ProductService } from '../services/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Product } from '../model/product';
import { DataService } from '../services/data.service'; 
import { NgxSpinnerService } from "ngx-spinner";  

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  userId = localStorage.getItem('userId');
  public products: Product[];
  title = 'Product List';
  authToken:any;
  msgString: any;
  errString: any;
  orderMongo : Order = new Order();
  curIndex: number;
  noRecords: boolean;
  constructor(private _productService:ProductService, private dataService: DataService,
    private formBuilder: FormBuilder, private router: ActivatedRoute, private route: Router,  
    private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.msgString = "";
    this.noRecords = false;
    this.authToken  = localStorage.getItem('authToken'); 
    this.router.queryParams.subscribe(params => {
      this.SpinnerService.show();  
      // console.log('params', params);      
      if(params && params["productByTitle"]) {
        this.dataService.searchProductByTitle(params["productByTitle"], this.authToken).subscribe((response) => {
        // console.log(response);
        this.products = response;
        this.noRecords = this.products.length > 0? false : true;
        this.SpinnerService.hide();  
      });
      } else {
        this._productService.getAllProducts(this.authToken).subscribe(data => {
          this.products = data;
          this.noRecords = this.products.length > 0? false : true;
          this.SpinnerService.hide();  
        });
      }
      
      // console.log('productByTitle', this.products);
      // console.log('noRecords', this.noRecords);      
    });    
  }

 placeOrder(product:Product, orderQuantity:number, index) {
      this.curIndex = index;
      const authToken = localStorage.getItem('authToken');
      if(orderQuantity && orderQuantity <= product.productQuantity && orderQuantity > 0) {

        if (authToken) {          
          this.orderMongo.userId=this.userId;  
          this.orderMongo.productId=product.productId;  
          this.orderMongo.quantity=orderQuantity;    
          this._productService.placeOrder(this.orderMongo,authToken).subscribe(data =>{
          this.msgString = data['message'];
        },
        (error) => {                             
          console.error('error caught in component', error)
          this.errString = error.message;
        });
      }else if(authToken) {
        this.route.navigate(['/']);
      } 
    }
  }
}