import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Product } from '../model/product';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  msgString: string;
  products: Product[];
  searchData: Product[];
  selectedProduct: Product[];
  constructor(private dataService: DataService, private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.dataService.getProducts().subscribe(products => {
      this.products = products
      this.dataService.productsData = products
    });
  this.selectedProduct = null;
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.msgString = params['msgString'] || '';
        if(params['msgString']) {
          setTimeout(() => {
            this.router.navigate(['/']);
          }, 5000);
        }
      });
  }

  getSearchEventData(event) {
    this.searchData = event;
    this.msgString = "";
  }

  getSelectedProduct(product) {
    this.msgString = "";
    this.searchData = null;
    this.selectedProduct = product;
  }
}
