import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterUserComponent } from './register-user/register-user.component';
import { LoginComponent } from './login/login.component';
import { ProductComponent } from './product/product.component';
import { OrderComponent } from './order/order.component';
import { AddProductComponent } from './add-product/add-product.component';
import { AuthGuardService } from './services/auth-guard.service';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path: '', component: HomeComponent },
  {path: 'register-user', component: RegisterUserComponent },
  {path: 'placeOrder', component: ProductComponent, canActivate: [AuthGuardService] },
  // {path: 'product/:id', component: ProductComponent },
  // {path: 'products', component: ProductComponent },
  {path: 'addproduct', component: AddProductComponent, canActivate: [AuthGuardService] },
  {path: 'login-user', component: LoginComponent },
  {path: 'orders', component : OrderComponent, canActivate: [AuthGuardService] } //, canActivate: [AuthGuardService]
  // { path: '**', component:  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
