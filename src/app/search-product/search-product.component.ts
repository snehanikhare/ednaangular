import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormControl } from '@angular/forms';
import { Product } from '../model/product';
import { Observable, Subject } from 'rxjs';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.css']
})
export class SearchProductComponent implements OnInit {
  results: Product[] = [];
  queryField: FormControl = new FormControl();
  searchRes: Product[];
  products$:Observable<any>;

  @Output() searchEventData = new EventEmitter<Product[]>();

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.getAllProducts();  
  }

  getAllProducts() {
    this.dataService.getProducts().subscribe(response => {
      this.results = response;
    });
  }

  searchProductByTitle(query) {
    this.searchRes = [];
    if(query) {
      this.results.filter((product) => {
        // console.log(query);
        let title = product.productTitle? product.productTitle.toLowerCase().replace(/-|\s/g,"") : product.productTitle;
        // console.log(title);
        if(title && title.includes(query.toLowerCase().replace(/-|\s/g,""))) {          
          this.searchRes.push(product);
        }
      });
    }
      this.searchEventData.emit(this.searchRes);
  }

  searchProduct(title) {
    // console.log('title', title);
    const searchKeyword = title? title : '';
    if(searchKeyword && searchKeyword !== '') {
      let navigationExtras: NavigationExtras = {
        queryParams: {
            "productByTitle": searchKeyword
        }
      };
      this.router.navigate(['/placeOrder'], navigationExtras);
    } else {
      this.router.navigate(['/placeOrder']);
    }    
  }
}

