import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from '../services/data.service';
import { Product } from '../model/product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  myForm: FormGroup;
  msgString: string;
  errString: any;
  constructor(private formBuilder: FormBuilder,
    private dataService: DataService, private route: Router) { }

  ngOnInit(): void {
    this.msgString = "";
    this.myForm = this.formBuilder.group({
      productTitle: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(20),Validators.pattern('[a-zA-Z0-9 ]*') ]],
      productQuantity: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.maxLength(2), Validators.max(99), Validators.min(1)]], // Validators.pattern('[0-9]*')]
      productDesc: ['', []],
      productPrice: ['', [Validators.required]],
      productImg: ['', [Validators.required]]
    });
  }

  onSubmit(myForm) {
    const authToken = localStorage.getItem('authToken');
    if (!this.myForm.invalid && authToken) {
      const product = this.myForm.value;
      product.productPrice = '$' + product.productPrice;
      this.dataService.addProduct(product, authToken).subscribe(response => {
        this.msgString = response;
        this.msgString = "Added product successfully!"
        this.myForm.reset();
        this.route.navigate(['/'], { queryParams: { msgString: this.msgString } });
      },
      (error) => {                              //Error callback
        console.error('error caught in component', error)
        this.errString = error.message;
      });
    } else if(authToken) {
      this.route.navigate(['/']);
    }
  }

}
