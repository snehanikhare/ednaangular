// import { LoginService } from './../app/services/LoginService';
export const environment = {
  production: false,  
   baseUrl : 'http://3.133.153.24:8091/edna-poc/eDNA',
  // baseUrl : 'http://localhost:8084/eDNA',
  
  webUrl : {
    users: '/users',
    placeOrder:'/ednaMongo/v1/placeOrder',
    listOrdersbyUserId:'/ednaMongo/v1/listOrdersbyUserId/',
    searchOrder:'/ednaMongo/v1/searchOrder/',
    login:'/login',
    addProduct: '/product/addProductMongo',
    getProducts: '/product/getProducts',
    getProductsByTitle: '/product/getProductMongo'
  }
};
 